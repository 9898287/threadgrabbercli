# threadgrabber

### Download images and videos from a 4chan thread

![Video](https://i.imgur.com/HFoGcrv.mp4)

## Usage:

`threadgrabbercli <thread url> <destination folder> <extension 1> <extension 2> <extension 3>...` 

**Note that if you don't provide the list of extensions, 
it will download all the images/videos into the destination folder**

## Building

* `dub build -b release`

## Dependancies

* `libcurl`