import std.stdio : writeln, writefln, stderr;

private void assertEq(T)(inout auto ref T lhs, inout auto ref T rhs, int line = __LINE__)
{
	debug
	{
		if (lhs != rhs)
		{

			stderr.writefln("Assertion failure in line %s:\n\tlhs: %s\n\trhs: %s", line, lhs, rhs);
		}
	}
	assert(lhs == rhs);
}

private struct Link
{
	string board;
	string thread;

	this(string uri)
	{
		int dashCount;
		foreach (c; uri)
		{
			if (c == '/')
				++dashCount;
			if (dashCount == 3 && c != '/')
				this.board ~= c;
			else if (dashCount == 5 && c >= '0' && c <= '9')
				this.thread ~= c;
			if (c == '#')
				break;
		}
	}

	string getJSONAPI() inout
	{
		import std.string : format;

		return format!"https://a.4cdn.org/%s/thread/%s.json"(board, thread);
	}
}

unittest
{
	auto link = Link(`https://boards.4channel.org/g/thread/76298934#p76298934`);
	assertEq(link.board, "g");
	assertEq(link.thread, "76298934");
	assertEq(link.getJSONAPI(), `https://a.4cdn.org/g/thread/76298934.json`);
}

private string getMediaLink(T)(inout auto ref T board, string filename, T ext)
{
	// https://is2.4chan.org/g/1591830015154.png
	import std.string : format;

	return format!"https://i.4cdn.org/%s/%s%s"(board, filename, ext);
}

unittest
{
	assertEq(getMediaLink("g", 1591830015154, ".png"), `https://is2.4chan.org/g/1591830015154.png`);
}

private synchronized class Listener
{

	private size_t downloaded, toDownload;

	this(size_t toDownload)
	{
		this.toDownload = toDownload;
	}

	void addOne(string name)
	{
		cast() downloaded += 1;

		writefln!"Downloaded %s (%s of %s)."(name, cast() downloaded, cast() toDownload);
	}

}

private void download(shared Listener listener, string uri, string path, string name, string ext)
{
	import std.net.curl : curlDownload = download;

	curlDownload(uri, path ~ "/" ~ name ~ ext);
	listener.addOne(name ~ ext);
}

void main(in string[] args)
{
	if (args.length < 3)
	{
		stderr.writeln(
				"Arguments missing. Usage: [program] [thread] [location] [list of space separated extensions]");
		import core.stdc.stdlib : exit, EXIT_FAILURE;

		exit(EXIT_FAILURE);
	}

	const string downloadFolder = (string dir) {
		import std.file : exists, mkdir, getcwd;

		if (!dir.exists())
			mkdir(getcwd() ~ "/" ~ dir ~ "/");
		return dir;
	}(args[2]);

	import std.net.curl : get;
	import std.json : parseJSON, JSONValue, JSONType;

	auto l = Link(args[1]);
	const JSONValue j = l.getJSONAPI().get().parseJSON();
	const JSONValue[] downloadLinks = (JSONValue jv) {
		import std.algorithm : splitter, filter, canFind;
		import std.range : array;

		// dfmt off
		return jv["posts"]
				.array() // this is a method of JSONValue
				.filter!(node => "tim" in node)
				.filter!(node => args.length > 3 
					? args[3 .. $].canFind(node["ext"].str()[1 .. $]) 
					: true
				).array(); // std.range: array
		// dfmt on
	}(j);

	auto listener = new shared Listener(downloadLinks.length);

	import std.parallelism : parallel;

	foreach (post; downloadLinks.parallel())
	{
		synchronized
		{
			import std.conv : to;

			const string name = post["tim"].integer.to!string, ext = post["ext"].str;
			listener.download(getMediaLink(l.board, name, ext), downloadFolder, name, ext);
		}
	}
	writeln("All downloads finished!");
}
